// dual parameters passing to old way function
const oldFunc = new Function('name,age',"return 'hello '+name+' your age is '+age")

console.log(oldFunc("ramu","20"))

const oldFunc_1 = new Function('name','age',"return 'hello '+name+' your age is '+age")

console.log(oldFunc_1("ramu","30"))

// new way function

const newFunc = function(name,age){
    return "hello "+ name +' your age is '+age
}

console.log(newFunc("ramu","20"))

const newFunc_1 = function(name,age){
    return "hello "+ name +' your age is '+age
}

console.log(newFunc_1("ramu","30"))